import React from "react";
import { TouchableOpacity, Text, StyleSheet } from "react-native";

const Boton = (props) => {
    const { onPress, text } = props
    return (
        <TouchableOpacity 
            style = { styles.buttonContainer }
            onPress = { onPress }
        >
            <Text 
                style = { styles.buttonText }
            > 
                { text }
            </Text>
        </TouchableOpacity>
    )
}


const styles = StyleSheet.create({
    buttonContainer: {
        width: '200px',
        backgroundColor: 'white',
        marginBottom: 20,
        paddingHorizontal: 30,
        paddingVertical: 20,
    },

    buttonText: {
        color: 'black',
    },
})

export default Boton
