import React from "react";
import { View, Text } from "react-native";
import Boton from "../components/Boton";

const Home = ({ navigation }) => {
    return (
        <View>
                <Boton 
                    text = "Ir a Inicio" 
                    onPress = { () => {
                        navigation.navigate('Profile', {
                            nombre: 'Mijael',
                            apellido: 'Rocha'
                        })
                    } }
                />
        </View>
    )
}

export default Home